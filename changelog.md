#Hide Sidebar
######Version 1.2.1 (15th February 2019)
- Added support to Question2Answer 1.8
- Added support for Lion theme
- Fixed minor issues

######Version 1.2 (older)
- Fixed conditional bug
- Removed checkbox for Custom
- Hide sidebar on defined custom developed page (new)
- Hide Sidebar for Custom Home Content Page 
- Now CSS applied to all themes built on Question2Answer standards
- Modified option keys with q2am_ prefix to reduce conflict with other option keys

######Version 1.1 (older)
>no record available

######Version 1.0 (older)
>no record available
